module github.com/belitre/helm-push-artifactory-plugin

go 1.14

require (
	github.com/Masterminds/semver v1.5.0 // indirect
	github.com/chartmuseum/helm-push v0.7.1
	github.com/gobwas/glob v0.2.3 // indirect
	github.com/jfrog/jfrog-client-go v0.8.1
	github.com/spf13/cobra v1.2.1
	github.com/stretchr/testify v1.7.0
	gopkg.in/yaml.v2 v2.4.0
	k8s.io/client-go v11.0.0+incompatible // indirect
	k8s.io/helm v2.16.3+incompatible
	oras.land/oras-go v1.1.1
)
